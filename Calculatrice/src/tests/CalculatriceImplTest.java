package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import calculatrice.Calculatrice;
import calculatrice.CalculatriceImpl;

public class CalculatriceImplTest {

	@Test
	public void testAddition() {
		Calculatrice calc = new CalculatriceImpl();
		int a, b, res;
		a = 5;
		b = 5;
		res = a + b;
		assertTrue("a et b positif", calc.addition(a, b) == res);
	}

	@Test
	public void testDivision() {
		Calculatrice calc = new CalculatriceImpl();
		int a, b, res;
		a = 5;
		b = 5;
		res = a / b;
		try {
		assertTrue("a et b positif", calc.division(a, b) == res);
		}
		catch (Exception e) {
		}
		
	}

	@Test
	public void testMultiplication() {
		Calculatrice calc = new CalculatriceImpl();
		int a, b, res;
		a = 5;
		b = 5;
		res = a * b;
		assertTrue("a et b positif", calc.multiplication(a, b) == res);
	}

	@Test
	public void testSoustraction() {
		Calculatrice calc = new CalculatriceImpl();
		int a, b, res;
		a = 5;
		b = 5;
		res = a - b;
		assertTrue("a et b positif", calc.soustraction(a, b) == res);
	}

}
