package calculatrice;

public class CalculatriceImpl implements Calculatrice {

	@Override
	public int addition(int a, int b) {
		int res = a;
		if (b > 0) { 
			while (b-- != 0) {
				res++;
			}
		}
		else if ( b < 0) {
			while (b++ !=0) {
				res--;
			}
		}
		
		return res;
	}

	@Override
	public float division(int a, int b) throws Exception {
		// Initialisation variables
		int negatif = 0;
		int add = 0;
		int res = 0;
		
		// Test a < 0 pour renvoyer le bon signe
		if (a < 0) {
			a = -a;
			negatif = -1;
		}
		
		else {
			negatif = 1;
		}
		
		// Test b > 0 pour renvoyer le bon signe
		if (b > 0) { 			
			while (add != a) {
				add = addition (add, b);
				
				res++;
			}
		}
		else if ( b < 0) {
			b = -b;
			while (add != a) {
				add = addition (add, b);
				res++;
			}
			
			return multiplication(-res, negatif);			
		}
		
		else
			throw new Exception ("Division par 0");
		
		return multiplication(res, negatif);
	}

	@Override
	public int multiplication(int a, int b) {
		int res = 0;
		if (b > 0) { 
			while (b-- != 0) {
				res = addition (res, a);
			}
		}
		else if ( b < 0) {
			b = -b;
			while (b-- !=0) {
				res = addition (res, a);
				
				return -res;
			}
		}
		
		return res;
	}

	@Override
	public int soustraction(int a, int b) {
		int res = a;
		if (b > 0) { 
			while (b-- != 0) {
				res--;
			}
		}
		else if ( b < 0) {
			while (b++ !=0) {
				res++;
			}
		}
		
		return res;
	}

}
