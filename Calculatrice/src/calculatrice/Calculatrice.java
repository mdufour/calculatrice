package calculatrice;

public interface Calculatrice {

	public int addition (int a, int b);
	public int soustraction (int a, int b);
	public int multiplication (int a, int b);
	public float division (int a, int b) throws Exception;
}
